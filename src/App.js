import React, { Component, Fragment } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import ContactsList from "./containers/ContactsList/ContactsList";
import AddContact from "./components/Contacts/AddContact/AddContact";
import EditContact from "./components/Contacts/EditContact/EditContact";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Switch>
                    <Route path="/" exact component={ContactsList}/>
                    <Route path="/add" component={AddContact}/>
                    <Route path="/edit/:id" component={EditContact}/>
                    <Route render={() =>  <h1>Not Found</h1>}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
