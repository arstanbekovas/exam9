import React, {Component} from 'react';
import {connect} from "react-redux";

import './AddContact.css';
import {addNewContact} from "../../../store/actions/contacts";
import NavLink from "react-router-dom/es/NavLink";

class AddContact extends  Component {
    state = {
        contact: {},
        loading: false
    };

    contactCreateHandler = e => {
       this.props.onContactAdded(this.state.contact);
       this.props.history.replace('/');
    };

    contactValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                contact: {...prevState.contact, [e.target.name]: e.target.value}
            }
        });
    };

    render(){
        return(
            <form className="ContactAdd">
                <h1>Add new dish</h1>
                <input type="text" name="name" placeholder="Enter a name"
                       value={this.state.contact.name} onChange={this.contactValueChanged}/>
                <input type="number" name="phone" placeholder="Enter phone number"
                          value={this.state.contact.phone} onChange={this.contactValueChanged}
                />
                <input type="text" name="email" placeholder="Enter email"
                          value={this.state.contact.email} onChange={this.contactValueChanged}
                />
                <input type="text" name="url" placeholder="Enter photo url"
                       value={this.state.contact.url} onChange={this.contactValueChanged}
                />
                <div>Photo preview </div>
                <img className='photo' src={this.state.contact.url} alt=""/>
                <div>
                    <button onClick={this.contactCreateHandler}>Save</button>
                    <NavLink to={'/'}  className='Back'>Back to Contacts</NavLink>
                </div>


            </form>
        )
    }
}


const mapStateToProps = state => {
    return {
        contacts: state.contacts.contacts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onContactAdded: (contactName) => dispatch(addNewContact(contactName)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddContact);