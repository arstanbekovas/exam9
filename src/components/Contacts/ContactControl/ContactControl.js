import React from 'react';
import './ContactControl.css';

const ContactControl = props => {
    return (
        <div onClick={props.click} className='ContactControl'>
            <img className='pic' src={props.pic}/>
            <div className='name'>{props.name}</div>
          </div>
    )
};

export default ContactControl;