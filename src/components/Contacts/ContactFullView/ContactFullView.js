import React from 'react';
import './ContactFullView.css';
import NavLink from "react-router-dom/es/NavLink";

const ContactFullView = props => {
    return (
        <div className='ContactFullView'>
            <img className='pic' src={props.pic}/>
            <div className='name'>{props.name}</div>
            <div className='phone'>Phone: {props.phone}</div>
            <div className='email'>Email: {props.email}</div>
            <NavLink to={'/'} onClick={props.delete} className='delete'>Delete</NavLink>
            <NavLink to={'/edit/' + props.id} className='edit'>Edit</NavLink>
        </div>
    )
};

export default ContactFullView;