import React, {Component} from 'react';
import './EditContact.css'
import {editContact, showOneContact} from "../../../store/actions/contacts";
import {connect} from "react-redux";
import NavLink from "react-router-dom/es/NavLink";

class EditContact extends Component {
    state = {
        contact: {},
        loading: false
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.onOneContactShown(id);
    }

    componentWillReceiveProps(nextProps) {
        this.state.contact = {...nextProps.selectedContact};
    }

    contactValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                contact: {...prevState.contact, [e.target.name]: e.target.value}
            }
        });
    };

    saveContact = (event) => {
        const id = this.props.match.params.id;
        const contactData = this.state.contact;
        event.preventDefault();
        this.props.onContactEdited(id, contactData);
        this.props.history.replace('/');

    };

    render() {

        return (
            <form className="EditContact">
                <h1>Edit a dish</h1>
                <input type="text" name="name" placeholder="Enter a name"
                       value={this.state.contact.name} onChange={this.contactValueChanged}/>
                <input type="number" name="phone" placeholder="Enter phone number"
                       value={this.state.contact.phone} onChange={this.contactValueChanged}
                />
                <input type="text" name="email" placeholder="Enter email"
                       value={this.state.contact.email} onChange={this.contactValueChanged}
                />
                <input type="text" name="url" placeholder="Enter photo url"
                       value={this.state.contact.url} onChange={this.contactValueChanged}
                />
                <div>Photo preview </div>
                <img className='photo' src={this.state.contact.url} alt=""/>
               <div>
                   <button onClick={this.saveContact}>Save</button>
                   <NavLink to={'/'}  className='Back'>Back to Contacts</NavLink>
               </div>

            </form>
        )
    }s
}

const mapStateToProps = state => {
    return {
        selectedContact: state.contacts.selectedContact
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onContactEdited: (id, contactData) => dispatch(editContact(id, contactData)),
        onOneContactShown: (id) => dispatch(showOneContact(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);