import React, {Component, Fragment} from 'react';
import './ContactsList.css';
import ContactControl from "../../components/Contacts/ContactControl/ContactControl";
import {connect} from 'react-redux';
import {
    deleteContact, editContact, showContacts,
    showOneContactSuccess
} from "../../store/actions/contacts";
import {NavLink} from "react-router-dom";
import Modal from "../../components/UI/Modal/Modal";
import ContactFullView from "../../components/Contacts/ContactFullView/ContactFullView";



class ContactsList extends Component {
    state = {
        contacts:{},
        showModal: false,
        selectedContact: ''
    };

    componentDidMount() {
        this.props.onShowContacts();
    };


    showModal = (event, id) => {
        event.preventDefault();
        this.props.history.replace('/');
        this.setState({showModal: true, selectedContact:id });
        this.props.onShowOneContact(id)
    };

    purchaseCancelHandler = () => {
        this.setState({showModal: false});
    };

    contactEditHandler = (id ) => {
        this.props.onContactEdited(id);
        console.log('edit id has',id);

        // this.state.contact = {...nextProps.selectedContact};

    };
    contactDeleteHandler = (id) => {
        this.props.onContactDeleted(id);
        this.props.history.replace('/');
        this.purchaseCancelHandler();
    };

    render() {
        console.log(this.state.selectedContact.id);
        let modalContent;
        if(this.state.selectedContact) {
            modalContent = (
                <ContactFullView
                    id={this.props.contacts[this.state.selectedContact].id}
                    name={this.props.contacts[this.state.selectedContact].name}
                    phone={this.props.contacts[this.state.selectedContact].phone}
                    email={this.props.contacts[this.state.selectedContact].email}
                    pic={this.props.contacts[this.state.selectedContact].url}
                    delete={() => this.contactDeleteHandler(this.props.selectedContact)}
                    edit={() => this.props.contactEditHandler(this.props.selectedContact)}
                />
            )
        } else {
            modalContent = null;
        }
        return (
            <Fragment>
                <h1>Contacts</h1>
                <NavLink className='button' to={'/add'}>Add new Contact</NavLink>
                <div>
                    {Object.keys(this.props.contacts).map(contactId => (
                        <ContactControl
                            click={(event) => this.showModal(event, contactId)}
                            key={contactId}
                            name={this.props.contacts[contactId].name}
                            pic={this.props.contacts[contactId].url}
                        />
                    ))}
                </div>
                <Modal
                    show={this.state.showModal}
                    closed={this.purchaseCancelHandler} >
                    {modalContent}
                </Modal>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts.contacts,
        selectedContact: state.contacts.selectedContact,
        editContact: state.contacts.editContact,
     }
};

const mapDispatchToProps = dispatch => {
    return {
        onShowContacts: () => dispatch(showContacts()),
        onContactDeleted: (contactId) => dispatch(deleteContact(contactId)),
        onContactEdited: (contactId) => dispatch(editContact(contactId)),
        onShowOneContact: (contactId) => dispatch(showOneContactSuccess(contactId))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactsList);