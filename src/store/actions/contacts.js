import * as actionTypes from './actionTypes';
import axios from "../../axios-contact";


export const contactRequest = () => {
    return {type: actionTypes.CONTACT_REQUEST};
};
export const contactSuccess = (info) => {
    return {type: actionTypes.CONTACT_SUCCESS, info};
};

export const contactError = (error) => {
    return {type: actionTypes.CONTACT_ERROR, error};
};

export const showContacts = () => {
    return dispatch => {
        dispatch(contactRequest());
        axios.get('/contacts.json').then((response) => {
            console.log(response.data);
            dispatch(contactSuccess(response.data));
        }, error => {
            dispatch(contactError(error));
        });
    }
};
export const deleteContact = (id) => {
    return dispatch => {
        dispatch(contactRequest());
        axios.delete(`/contacts/${id}.json`).then((response) => {
            console.log(response.data);
            dispatch(showContacts())
        }, error => {
            dispatch(contactError(error));
        });
    }
};

export const showOneContactSuccess = (contact) => {
    return {type: actionTypes.SHOW_ONE_CONTACT_SUCCESS, contact};
};

export const showOneContact = (id) => {
    return dispatch => {
        axios.get('/contacts/' + id + '.json').then(response => {
            const contact = response.data;
            dispatch(showOneContactSuccess(contact))
        })
    }
};

export const addNewContact = (info) => {
    return dispatch => {
        dispatch(contactRequest());
        axios.post('/contacts.json', info).then((response) => {
            console.log(response.data);
            dispatch(showContacts())
        }, error => {
            dispatch(contactError(error));
    });
}};

export const editContact = (id, contactData) => {
    return dispatch => {
        dispatch(contactRequest());
        axios.patch(`/contacts/${id}.json`, contactData).then((response) => {
            console.log(response.data);
            dispatch(contactSuccess(response.data));
        }, error => {
            dispatch(contactError(error));
        });
    }
};

