import * as actionTypes from "../actions/actionTypes";


const initialState = {
    contacts: {},
    selectedContact: {},
    editedContact: null,

};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.CONTACT_SUCCESS:
            console.log(action.info );
            return {...state, contacts: action.info};

        case actionTypes.SHOW_ONE_CONTACT_SUCCESS:
            return {...state, selectedContact: action.contact};
        default:
            return state;
    }

};

export default reducer;